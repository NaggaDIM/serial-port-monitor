#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import glob
import serial
import serial.tools.list_ports


class Controller():
	def __init__(self, ui):
		self.ui = ui

	def get_serial_ports_list(self):
		ports = {}
		for port in list(serial.tools.list_ports.comports()):
			ports[port.device] = '{} - {}'.format(port.device, port.description)
		return ports

	def get_speed_list(self): return [50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200]

	def get_databit_count_list(self): return [serial.FIVEBITS, serial.SIXBITS, serial.SEVENBITS, serial.EIGHTBITS]

	def get_stopbit_count_list(self): return [serial.STOPBITS_ONE, serial.STOPBITS_ONE_POINT_FIVE, serial.STOPBITS_TWO]