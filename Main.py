#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from PyQt5.QtWidgets import QApplication
from GUI import MainUI


if __name__ == '__main__':
	app = QApplication(sys.argv)
	main_ui = MainUI(sys.argv)
	sys.exit(app.exec_())