#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QComboBox, QPushButton
from Controller import Controller


class MainUI(QMainWindow):
	def __init__(self, parent=None):
		super().__init__()
		self.controller = Controller(self)
		self.window_widget = MainWidget(self)
		
		self.setCentralWidget(self.window_widget)
		self.show()

class MainWidget(QWidget):
	def __init__(self, parent):
		super(MainWidget, self).__init__(parent)
		self.parent = parent
		self.layout = QVBoxLayout(self)

		self.init_ui()

	def init_ui(self):
		config_layout = QHBoxLayout(self)
		config_layout.addWidget(QLabel('Порт: '))
		ports = QComboBox(self)
		self.fill_ports(ports, self.parent.controller.get_serial_ports_list())
		# items = 
		# for item in items:
		# 	print(items[item])
		# 	ports.addItem(item, userData=items[item])
		# ports.addItems(self.parent.controller.get_serial_ports_list())
		config_layout.addWidget(ports)

		print(ports.itemData(ports.currentIndex()))


		config_layout.addWidget(QLabel('Скорость: '))
		speeds = QComboBox(self)
		# speeds.addItems(self.parent.controller.get_speed_list())
		self.fill_int_list(speeds, self.parent.controller.get_speed_list())
		config_layout.addWidget(speeds)

		config_layout.addWidget(QLabel('Бит данных: '))
		databits = QComboBox(self)
		# databits.addItems(self.parent.controller.get_databit_count_list())
		self.fill_int_list(databits, self.parent.controller.get_databit_count_list())
		config_layout.addWidget(databits)

		config_layout.addWidget(QLabel('Стоп бит: '))
		stopbits = QComboBox(self)
		# stopbits.addItems(self.parent.controller.get_stopbit_count_list())
		self.fill_int_list(stopbits, self.parent.controller.get_stopbit_count_list())
		config_layout.addWidget(stopbits)

		config_layout.addStretch(1)

		self.layout.addLayout(config_layout)
		self.layout.addStretch(1)

	def fill_ports(self, ports_widget, items):
		ports_widget.clear()
		for i in items:
			ports_widget.addItem(items[i], userData=i)

	def fill_int_list(self, list_widget, items):
		list_widget.clear()
		for i in items:
			list_widget.addItem(str(i), userData=i)
